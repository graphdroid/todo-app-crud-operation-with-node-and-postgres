import { postTodo, description } from "./src/postTodo.js"

const addBtn = document.getElementById('addBtn')

// Event listener for adding todos
addBtn.addEventListener('click', postTodo)

// Function for displaying todos
const getTodo = async () => {
    const dataTbl = document.getElementById('data')
    const noTodoPlaceholder = document.getElementById('noTodoPlaceholder')

    const todoUrl = 'http://localhost:3000/todos'

    let updateBtnClicked = 0
    let submitBtnClicked = 0

    try {
        const res = await fetch(todoUrl)
        const data = await res.json()
        
        // Placeholder when there is no todo
        if(data.length == 0) {
            noTodoPlaceholder.style.display = "inline"
        } else {
            noTodoPlaceholder.style.display = "none"
        }
        
        for (let i = 0; i < data.length; i++) {
            // Starting of template creating for holding todo data
            const tableRow = document.createElement('tr')
            const tableData = document.createElement('td')
            const dataHolder = document.createElement('div')

            const updateTodo = document.createElement('td')
            const updateBtn = document.createElement('button')
            const deleteTodo = document.createElement('td')
            const deleteBtn = document.createElement('button')

            dataTbl.appendChild(tableRow)
            tableRow.appendChild(tableData)
            tableData.appendChild(dataHolder)

            tableRow.appendChild(updateTodo)
            updateTodo.appendChild(updateBtn)

            tableData.setAttribute("style", "width: 20vw; text-align: justify;")

            updateBtn.setAttribute("id", "updateBtn")
            updateBtn.setAttribute("class", "btn btn-sm btn-primary ms-3")

            tableRow.appendChild(deleteTodo)
            deleteTodo.appendChild(deleteBtn)

            deleteBtn.setAttribute("class", "btn btn-sm btn-danger ms-1")
            
            updateBtn.innerHTML = "Update"
            deleteBtn.innerHTML = "&times"

            dataHolder.setAttribute('data-id', data[i].id)
            dataHolder.innerText = data[i].description
            // End of template creation

            // Update Button
            updateBtn.addEventListener('click', () => {
                const updateField = document.createElement('input')
                const submitBtn = document.createElement('button')
                const todoValue = tableData.innerText

                updateField.setAttribute("type", "text")
                description.setAttribute("disabled", "")
                addBtn.setAttribute("disabled", "")
                
                updateField.setAttribute("class", "rounded-1")
                submitBtn.setAttribute("class", "btn btn-sm btn-primary")

                updateField.id = "updateInput"
                submitBtn.id = "submitBtn"

                tableData.appendChild(updateField)
                updateTodo.appendChild(submitBtn)

                dataHolder.style.display = "none"
                updateBtn.style.display = "none"

                submitBtn.innerHTML = "Submit"

                updateField.value = todoValue

                updateBtnClicked += 1

                const disableFunc = () => {
                    updateField.style.display = "none"
                    submitBtn.style.display = "none"
                    dataHolder.style.display = "block"
                    updateBtn.style.display = "block"
                }

                if (updateBtnClicked == 2 && submitBtnClicked == 0) {
                    disableFunc()
                } else if (updateBtnClicked > 2 && submitBtnClicked == 0) {
                    disableFunc()
                }

                // Submit Button
                submitBtn.addEventListener('click', (e) => {
                    e.preventDefault()
                    const updatedValue = updateField.value

                    const updateTodo = async () => {

                        description.removeAttribute("disabled")
                        addBtn.removeAttribute("disabled")
                        
                        updateField.style.display = "none"
                        submitBtn.style.display = "none"
                        dataHolder.style.display = "block"
                        updateBtn.style.display = "block"

                        const id = dataHolder.getAttribute("data-id")
                        const idUrl = `http://localhost:3000/todos/${id}`
                        try {
                            await fetch(idUrl, {
                                method: 'PUT',
                                headers: { "Content-Type": "application/json" },
                                body: JSON.stringify({
                                    description: updatedValue
                                })
                            })
                        } catch (err) {
                            console.log(err.message)
                        }
                        location.reload()
                    }
                    updateTodo()
                    submitBtnClicked += 1
                })
            })

            // Delete Button
            deleteBtn.addEventListener('click', () => {
                const deleteTodo = async () => {
                    const id = dataHolder.getAttribute("data-id")
                    const idUrl = `http://localhost:3000/todos/${id}`
                    try {
                        await fetch(idUrl, {
                            method: 'DELETE'
                        })
                    } catch (err) {
                        console.log(err.message)
                    }

                    location.reload()
                }

                deleteTodo()
            })
        }
    } catch (err) {
        console.log(err.message)
    }
}

getTodo()