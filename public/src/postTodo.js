export const description = document.getElementById('description')

export async function postTodo(e) {
    const baseUrl = 'http://localhost:3000/'

    e.preventDefault()
    if(description.value == '') { return }
    try{
    await fetch(baseUrl,
        {
            method: 'POST',
            headers: {
                "Content-Type": 'application/json'
            },
            body: JSON.stringify({
                description: description.value
            })
        })
    location.reload()
    } catch (err) {
        console.error(err.message)
    }
}