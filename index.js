const express = require('express')
const cors = require('cors')
const pool = require("./db")
const app = express();

// Middleware
app.use(express.static('public'))
app.use(cors())
app.use(express.json())

// create a todo
app.post('/', async (req, res) => {
    const todo = req.body.description
    try {
        const newTodo = await pool.query (
            "INSERT INTO todo (description) VALUES ($1)",
            [todo]
        )
      res.json(newTodo.rows[0])
      } catch (err) {
            console.log(err.message)
        }
        console.log(todo)
})

// get all todos
app.get('/todos', async (req, res) => {
    try {
        const allTodos = await pool.query ("SELECT * FROM todo;") 
        res.json(allTodos.rows)
        console.log(allTodos.rows)
    } catch (err) {
            console.log(err.message)
        }
})

// update a todo
app.put("/todos/:id", async (req, res) => {
  try {
    const { id } = req.params
    const { description } = req.body
    await pool.query("UPDATE todo SET description = $1 WHERE id = $2", [description, id])
    res.json("Todo was updated!")
  } catch(err) {
    console.log(err.message)
  }
})

// delete a todo
app.delete("/todos/:id", async (req, res) => {
    try {
      const { id } = req.params;
      await pool.query("DELETE FROM todo WHERE id = $1", [id]);
      res.json("Todo is deleted")
    } catch (err) {
      console.log(err.message);
    }
})

app.listen(3000, () => {
    console.log("Server running on port 3000");
});