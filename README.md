# **Todo App - CRUD operation with Node and Postgres**

The todo app project is the simple project made while learning about node, postgres and how can we patch up all the files and resources to make to fully fledged app.
***
<br>

## **Dependencies**

<br>

**_express_** - A Node js framework

<code>npm i express</code>

**_pg_** - A npm package which helps us to connect with postgresql database and interact with it.

<code>npm i pg</code>

**_cors_** - Cross Origin Resource Sharing simply CORS is used for exporting and importing modules in client side and using the bootstrap cdn.

<code>npm i cors</code>

**_nodemon_** - Nodemon is used to automatically restart the node application.

<code>npm i -g nodemon</code>
***
<br>

## **Resources**

<br>

### **_Bootstrap 5.0 CDN_**

```HTML
<!-- CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
```
***